
 ▼ Resurrection Remix Nougat Ver 5.8.4 Changelog


 ▼ 08/20/2017


 ▪ project device/htc/msm8974-common/
 ▪ c3d0a28 msm8974-common: GestureHandler: Remove redundant DT2W toggle

 ▪ project frameworks/av/
 ▪ d38975e Revert "libstagefright: Use proper ctts offset"

 ▪ project frameworks/base/

 ▪ project hardware/ril-caf/
 ▪ d8290dd ril: Allow targets to disable Clang

 ▪ project kernel/google/marlin/
 ▪ fc16445 Revert "scripts: update user/host name"

 ▪ project kernel/htc/msm8974/
 ▪ 751f4ec [HACK] drivers: CwMcuSensor: Disable abs for uncalibrated gyroscope

 ▪ project packages/apps/AudioFX/

 ▪ project packages/apps/Eleven/

 ▪ project packages/apps/Jelly/

 ▪ project packages/apps/OmniSwitch/

 ▪ project packages/apps/Settings/
 ▪ 14513f5 Update s7 edge maintainer
 ▪ 3bb44bb Transfer maintainership of Galaxy S7 series to schwabe93

 ▪ project packages/apps/SetupWizard/

 ▪ project packages/apps/Snap/

 ▪ project packages/apps/Terminal/

 ▪ project packages/apps/Trebuchet/

 ▪ project packages/apps/UnifiedEmail/

 ▪ project vendor/opengapps/sources/arm/
 ▪ 930a79a Google (beta) arm-16 7.9.20.16.arm (nodpi)

 ▪ project vendor/opengapps/sources/arm64/
 ▪ 55ec72e Photos arm64-16 3.3.0.165520514 (213-240)

 ▪ project vendor/opengapps/sources/x86/
 ▪ 7e76e44 Allo x86-16 16.0.024_RC10 (x86_alldpi) (nodpi)

 ▼ 08/19/2017


 ▪ project build/
 ▪ 6a081e1 envsetup: stop jack server once build completed

 ▪ project device/htc/m8/
 ▪ 8eafee2 m8: init: Override GMS_CLIENTID_BASE for Verizon variant

 ▪ project device/htc/m8-common/
 ▪ d009b96 m8-common: releasetools: Remove property replacement for Verizon

 ▪ project packages/apps/CMParts/
 ▪ f763660 CMParts: Add strings for DCI-P3 color profile

 ▪ project packages/apps/Settings/
 ▪ 4dccd0d Changing Galaxy s7 edge Maintainer (#846)

 ▪ project vendor/opengapps/sources/all/
 ▪ 75b3f70 Gmail all-16 7.7.30.165668480.release (nodpi)

 ▪ project vendor/opengapps/sources/x86_64/
 ▪ d61a2cb Android System WebView x86_64-21 60.0.3112.107 (nodpi)

 ▼ 08/18/2017


 ▪ project device/qcom/sepolicy/
 ▪ c631819 Use set_prop() macro for property sets

 ▼ 08/17/2017


 ▪ project device/google/marlin/
 ▪ 8b5ef51 marlin: update fingerprint to NJH47F

 ▪ project device/htc/m8-common/
 ▪ 3a12a4b m8-common: Re-organize the list of common blobs
 ▪ 12f57de m8-common: Update extractors to match the latest templates

 ▪ project device/htc/msm8974-common/
 ▪ 934b636 msm8974-common: Add devicesettings as a dependency

 ▪ project kernel/google/marlin/
 ▪ 69c0d4d defconfig:apascual89 version
 ▪ ea6fcef sched: walt: fix window misalignment when HZ=300
 ▪ d8edaec include: asm-generic: do not hard code HZ to 100
 ▪ 324ce45 Merge tag 'android-7.1.2_r0.33' into nougat

 ▪ project kernel/htc/msm8974/
 ▪ 6da4d9a ANDROID: sdcardfs: override credential for ioctl to lower fs
 ▪ 79458ce net: bluetooth: Fix mismerges
 ▪ b17e6fd net: bluetooth: Partially reset to CAF

 ▪ project packages/apps/Settings/
 ▪ aca49e9 Add Co Maintainer for Jalebi (#845)

 ▼ 08/16/2017


 ▪ project device/htc/m8-common/
 ▪ 7e1a3f2 sepolicy: Clean up permissions

 ▪ project device/htc/msm8974-common/
 ▪ 2770ecd msm8974-common: Switch to common lid app (FlipFlap)

 ▪ project hardware/cyanogen/
 ▪ e2f8e45 livedisplay: SDM: Add support for DCI-P3 display mode

 ▪ project hardware/qcom/bt-caf/
 ▪ 328d8af Revert "Merge tag 'LA.UM.5.5.r1-04900-8x96.0' into HEAD"

 ▪ project kernel/htc/msm8974/
 ▪ 4b94983 drivers: hall_sensor: Report expected SW_LID keycode

 ▪ project packages/apps/Jelly/
 ▪ 21b9d05 Jelly: Check for onChanged in history data observer

 ▪ project packages/apps/Settings/
 ▪ 115a541 added helium (#843)

 ▼ 08/15/2017


 ▪ project kernel/htc/msm8974/
 ▪ 2c9d1cf drivers: media: camera_v2: Change compilation order of sensors

 ▪ project packages/apps/Jelly/
 ▪ 1ea2382 Jelly: Improve URL bar behavior

 ▪ project packages/apps/Settings/
 ▪ 7d771ca Fix i9500 maintainer dont show (#842)
 ▪ df1f26e Fix i9500 maintainer dont show (#840)

 ▼ 08/14/2017


 ▪ project frameworks/base/

 ▪ project kernel/htc/msm8974/
 ▪ b59fb7c ALSA: timer: Fix race between read and ioctl
 ▪ a5fbb91 ALSA: timer: Fix missing queue indices reset at SNDRV_TIMER_IOCTL_SELECT
 ▪ b8e1305 fs/exec.c: account for argv/envp pointers
 ▪ 7016388 prima: Trim extn capability to max supported in change station
 ▪ e0bcd01 ipv6: fix out of bound writes in __ip6_append_data()
 ▪ 5bb41df ipv6/dccp: do not inherit ipv6_mc_list from parent
 ▪ 54851ff sctp: do not inherit ipv6_{mc|ac|fl}_list from parent
 ▪ da44e69 ipv6: Check ip6_find_1stfragopt() return value properly.
 ▪ 6d69016 ipv6: Prevent overrun when parsing v6 header options
 ▪ e1244ea ashmem: remove cache maintenance support
 ▪ 8ec762a msm: kgsl: Fix kgsl memory allocation and free race condition
 ▪ 57da347 msm: camera: don't cut to 8bits for validating enum variable
 ▪ 14da8fc msm: camera: sensor: Add boundary check for cci master
 ▪ 12a4763 qseecom: add mutex around qseecom_set_client_mem_param
 ▪ 07049b9 crypto: ahash - Fix EINPROGRESS notification callback
 ▪ 846bb31 crypto: hash - Simplify the ahash_finup implementation
 ▪ 0b24bb3 crypto: hash - Pull out the functions to save/restore request
 ▪ 33bbfa2 crypto: hash - Fix the pointer voodoo in unaligned ahash
 ▪ f0005a7 crypto: ahash - Fully restore ahash request before completing
 ▪ ab60cff mm/mempolicy.c: fix error handling in set_mempolicy and mbind.
 ▪ 5276431 mm: fix anon vma naming
 ▪ 21e0cd3 net/packet: fix overflow in check for priv area size
 ▪ a6ca6b3 packet: handle too big packets for PACKET_V3
 ▪ 738b7e7 net/packet: fix overflow in check for tp_frame_nr
 ▪ c8df7a3 xfrm_user: validate XFRM_MSG_NEWAE incoming ESN size harder
 ▪ 4e31cea xfrm_user: validate XFRM_MSG_NEWAE XFRMA_REPLAY_ESN_VAL replay_window
 ▪ adc547a sctp: deny peeloff operation on asocs with threads sleeping on it
 ▪ 4582e08 irda: Fix lockdep annotations in hashbin_delete().
 ▪ 820617d xfrm: policy: check policy direction value
 ▪ d0f0acd qcdev: Check the digest length during the SHA operations
 ▪ 14e20aa timerfd: Protect the might cancel mechanism proper
 ▪ ba3a7e7 sysrq: FIQ and sysrq default deauthorized
 ▪ f78b8b4 BACKPORT: fiq_debugger: restrict access to critical commands.
 ▪ 871ae2c BACKPORT: f2fs: sanity check checkpoint segno and blkoff
 ▪ f01c1a3 UPSTREAM: f2fs: sanity check segment count
 ▪ e3b1d3b BACKPORT: f2fs: sanity check log_blocks_per_seg
 ▪ 29aeced BACKPORT: msm: mdss: Fix invalid dma attachment during fb shutdown
 ▪ 8443b71 msm: ipa: Fix for missing int overflow check in the refcount library
 ▪ dbaef65 ASoC: msm: qdsp6/qdsp6v2: check audio client pointer before accessing
 ▪ f2e5d22 ASoC: msm: qdsp6/qdsp6v2: validate audio client in callback
 ▪ 81da2f1 ASoC: msm: qdsp6/qdsp6v2: set pointer to NULL after free.
 ▪ aaca656 ASoC: soc: qdsp6: prevent risk of buffer overflow

 ▪ project packages/apps/DUI/

 ▪ project packages/apps/OmniSwitch/

 ▪ project packages/apps/Settings/
 ▪ 72deb12 Fix land device position (#839)
 ▪ f6cc7a7 Add Redmi 4X device maintainer (#838)

 ▪ project packages/services/OmniJaws/

